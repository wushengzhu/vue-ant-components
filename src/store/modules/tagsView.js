// router.js模块

const state = {
    visitedViews: [],
    cachedViews: []
}

const mutations = {
    AddVisitedView: (state, view) => {
        if (state.visitedViews.some(v => v.path === view.path)) return;
        state.visitedViews.push(
            Object.assign({}, view, { title: view.meta.title || 'no-name' })
        )
    },
    AddCachedView: (state, view) => {
        if (state.cachedViews.includes(view.name)) return;
        if (!view.meta.noCache) {
            state.cachedViews.push(view.name);
        }
    },

    DelVisitedView: (state, view) => {
        for (const [i, v] of state.visitedViews.entries()) {
            if (v.path === view.path) {
                state.visitedViews.splice(i, 1)
                break;
            }
        }
    },
    DelCacheView: (state, view) => {
        const index = state.cachedViews.indexOf(view.name)
        index > -1 && state.cachedViews.splice(index, 1);
    },
    DelOthersVisitedViews: (state, view) => {
        state.visitedViews = state.visitedViews.filter(v => {
            return v.path === view.path;
        })
    },
    DelOthersCacheViews: (state, view) => {
        const index = state.cachedViews.indexOf(view.name);
        if (index > -1) {
            state.cachedViews = state.cachedViews.slice(index, index + 1);
        } else {
            state.cachedViews = [];
        }
    },
    DelAllVisitedView: (state, view) => {
        const affixTags = state.visitedViews.filter(tag => tag.meta.affix)
        state.visitedViews = affixTags;
    },
    DelAllCacheViews: state => {
        state.cachedViews = [];
    },

    UpdateVisitedView: (state, view) => {
        for (let v of state.visitedViews) {
            if (v.path === view.path) {
                v = Object.assign(v, view);
                break;
            }
        }
    }
}

const actions = {
    addView ({ dispatch }, view) {
        dispatch('addVisitedView', view);
        dispatch('addCachedView', view);
    },
    addVisitedView ({ commit }, view) {
        commit('AddVisitedView', view);
    },
    addCachedView ({ commit }, view) {
        commit('AddCachedView', view);
    },

    delView ({ dispatch, state }, view) {
        return new Promise(resolve => {
            dispatch('delVisitedView', view);
            dispatch('delCachedView', view);
            resolve({
                visitedViews: [...state.visitedViews],
                cachedViews: [...state.cachedViews],
            })
        })
    },
    delVisitedView ({ commit, state }, view) {
        return new Promise(resolve => {
            commit('DelVisitedView', view)
            resolve([...state.visitedViews])
        })
    },
    delCachedView ({ commit, state }, view) {
        return new Promise(resolve => {
            commit('DelCachedView', view)
            resolve([...state.cachedViews])
        })
    },

    delOthersViews ({ dispatch, state }, view) {
        return new Promise(resolve => {
            dispatch('delOthersVisitedViews', view)
            dispatch('delOthersCachedViews', view)
            resolve({
                visitedViews: [...state.visitedViews],
                cachedViews: [...state.cachedViews]
            })
        })
    },
    delOthersVisitedViews ({ commit, state }, view) {
        return new Promise(resolve => {
            commit('DelOthersVisitedViews', view)
            resolve([...state.visitedViews])
        })
    },
    delOthersCachedViews ({ commit, state }, view) {
        return new Promise(resolve => {
            commit('DelOthersCachedViews', view)
            resolve([...state.cachedViews])
        })
    },

    delAllViews ({ dispatch, state }, view) {
        return new Promise(resolve => {
            dispatch('delAllVisitedViews', view)
            dispatch('delAllCachedViews', view)
            resolve({
                visitedViews: [...state.visitedViews],
                cachedViews: [...state.cachedViews]
            })
        })
    },
    delAllVisitedViews ({ commit, state }) {
        return new Promise(resolve => {
            commit('DelAllVisitedViews')
            resolve([...state.visitedViews])
        })
    },
    delAllCachedViews ({ commit, state }) {
        return new Promise(resolve => {
            commit('DelAllCachedViews')
            resolve([...state.cachedViews])
        })
    },

    updateVisitedView ({ commit }, view) {
        commit('UpdateVisitedView', view)
    }
}

export default {
    namespaced: true, // 为每个模块添加一个前缀名，保证模块命明不冲突 
    state,
    mutations,
    actions
}


import { get, post } from '../http';

export const saveUser = (data) => post('/api/Train/User/Save', data);

export const getUserList = (data) => post('/api/Train/User/GetList', data);

export const getUserById = (userId) => get('/api/Train/User/GetById', { userId: userId });

export const deleteUser = (userId) => get('/api/Train/User/Delete', { userId: userId });

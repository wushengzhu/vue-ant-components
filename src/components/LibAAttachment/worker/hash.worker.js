import SparkMD5 from 'spark-md5';

onmessage = function (event) {
  getFileHash(event.data);
};

function getFileHash({ file, chunkSize }) {
  const spark = new SparkMD5.ArrayBuffer();
  const reader = new FileReader();
  reader.readAsArrayBuffer(file);

  reader.addEventListener('loadend', () => {
    const content = reader.result;
    spark.append(content);

    const hash = spark.end();
    postMessage(hash);
  });

  reader.addEventListener('error', function _error(err) {
    postMessage(err);
  });
}

import { get, post } from '../http';
import request from '@/utils/interceptors';
import axios from 'axios';

export const uploadImage = (data) => post('/api/Train/Image/Upload', data);

export const saveAttachment = (data) => post('/api/Train/Attachment/Save', data);

export const deleteAttach = (data) => get('/api/Train/Attachment/Delete', data);

export const getFileById = (data) => get('/api/Train/Attachment/GetFileUrl', data);

export const downloadAttach = (data, responseType = 'blob') =>
  // 如果直接使用拦截器封装的axios会出现文件损坏
  axios({
    url: '/api/Train/Attachment/Download',
    method: 'get',
    params: data,
    responseType: responseType,
  });

// export const uploadChunksAttachment = (data, config = { onUploadProgress: (e) => e }) =>
//   request({
//     url: `/api/Train/Attachment/UploadChunk`,
//     method: 'post',
//     data: data,
//     headers: {
//       'content-type': 'multipart/form-data',
//     },
//     onUploadProgress: config.onUploadProgress,
//   });

export const uploadAttachment = (url, data, config = { onUploadProgress: (e) => e }) =>
  request({
    url: `/api/Train/Attachment/${url}`,
    method: 'post',
    data: data,
    // headers: {
    //   'content-type': 'multipart/form-data',
    // },
    onUploadProgress: config.onUploadProgress,
  });

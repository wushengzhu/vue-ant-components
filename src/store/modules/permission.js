import { customRoutes } from '@/router/index'

function hasPermission (roles, route) {
    if (route.meta && route.meta.roles) {
        return roles.some(role => role.meta.roles.includes(role));
    } else {
        return true;
    }
}

const state = {
    routes: [],
    addRoutes: [],
}

const mutations = {
    setRoutes: (state, routes) => {
        state.addRoutes = routes;
        state.routes = customRoutes;
    }
}

const actions = {
    generateRoutes ({ commit }, roles) {
        return new Promise(resolve => {
            commit('setRoutes', []);
            resolve([]);
        })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
import axios from '@/utils/interceptors';

/**
 * get方法，对应get请求
 * @param {String} url [请求地址]
 * @param {Object} params [请求参数]
 */
export function get (url, params) {
  return new Promise((resolve, reject) => {
    axios
      .get(url, { params })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err.data);
      });
  });
}

/**
 * post方法，对应post请求
 * @param {String} url [请求地址]
 * @param {Object} params [请求参数]
 */
export function post (url, params, config = {}) {
  return new Promise((resolve, reject) => {
    axios
      .post(url, params, config)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err.data);
      });
  });
}

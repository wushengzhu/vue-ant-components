<p align="center">
	
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">vue-ant-components 0.1.0</h1>
<h4 align="center">基于Vue3+Nodejs+MongoDB封装日常后台管理组件</h4>
<p align="center">
	<a href="https://github.com/vuejs/core">
    <img src="https://img.shields.io/badge/Vue-v3.2.45-brightgreen">
    </a>
    <a href="https://github.com/nodejs">
     <img src="https://img.shields.io/badge/Nodejs-v14.17.0-brightgreen">
    </a>
	<a href="https://github.com/mongodb">
    <img src="https://img.shields.io/badge/MongoDB-v5.0.9-brightgreen">
    </a>
    <a href="https://www.npmjs.com/package/mongoose">
    <img src="https://img.shields.io/badge/mongoose-^6.7.0-brightgreen">
    </a>
    <a href="https://www.antdv.com/components/overview-cn/">
    <img src="https://img.shields.io/badge/AntDesignVue-^3.2.13-brightgreen">
    </a>
</p>

### 上传组件

🍓 **1 前端部分**

- [x] `上传附件`：可上传多个，上传将会触发多次进度条。
- [x] `批量下载`：选择需要下载的多个附件。
- [x] `批量删除`：选择需要删除的多个附件。
- [x] `文件下载`：可直接在列表下载对应附件。
- [x] `文件重命名`：重命名附件名称，直接在列表标题触发 input 修改。
- [x] `文件删除`：可直接在列表删除对应附件。
- [x] `文件预览`：由于 xlsx、pdf 插件的兼容性问题，本组件只支持 docx（插件实现）、pdf（浏览器预览）的预览。
- [x] `文件上传进度`：监听每个分片上传进度，及时渲染当前进度数值。

🍓 **2 后端部分**

- [x] `验证附件分片api`：验证是否已存在已上传的分片，不用再次请求，只上传未上传的分片。api 响应回来的是两个字段：是否已上传所有分片，以及已上传的分片数组。
- [x] `上传附件分片api`：附件验证后触发的 api，作用于上传分片。
- [x] `合并附件分片api`：所有附件分片上传后触发的 api，作用于合并附件分片。
- [x] `删除附件api`：删除附件信息，以及存在本地服务器的文件。
- [x] `获取附件列表api`：获取所有附件。
- [x] `获取附件信息api`：获取存到数据库的一些附件信息。
- [x] `下载附件api`：需要响应种类为'blob'。

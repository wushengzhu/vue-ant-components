export default class UploadStatus {
  /**
   * 等待状态
   */
  static wait = 0;
  /**
   * 暂停状态
   */
  static pause = 1;
  /**
   * 上传状态
   */
  static uploading = 2;
  /**
   * 错误状态
   */
  static error = 3;
  /**
   * 完成状态
   */
  static done = 4;
}

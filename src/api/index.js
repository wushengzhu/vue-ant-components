import { post } from './http';

export * from './modules/user';
export * from './modules/file';

export const getPageData = (url, data) => post(url, data);

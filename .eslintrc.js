module.exports = {
  // 代码的校验
  root: true,
  env: {
    node: true,
  },
  plugins: ['prettier'], // prettier 肯定要是最后一个，确保覆盖与eslint中冲突的配置
  extends: ['plugin:vue/vue3-essential', 'eslint:recommended', 'plugin:prettier/recommended'],
  parserOptions: {
    // parser: 'babel-eslint',
    // 不用babel-eslint，用最新的babel
    parser: '@babel/eslint-parser',
    ecmaVersion: 6, //emcaVersion用来指定你想要使用的 ECMAScript 版本
    requireConfigFile: false,
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'prettier/prettier': 'warn',
    'no-unused-vars': 'warn',
  },
  overrides: [
    {
      files: ['**/__tests__/*.{j,t}s?(x)', '**/tests/unit/**/*.spec.{j,t}s?(x)'],
      env: {
        jest: true,
      },
    },
  ],
};

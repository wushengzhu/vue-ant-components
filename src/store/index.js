import { createStore } from 'vuex';
import getters from './getters'
/**
 * require.context：webpack的一个api
 * @param directory {String} -读取文件的路径
 * @param useSubdirectories {Boolean} -是否遍历文件的子目录
 * @param regExp {RegExp} -匹配文件的正则
 * @returns 返回值是一个function，有三个属性：resolve 、keys、id
 */
const modulesFiles = require.context('./modules', true, /\.js$/);
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  // \w匹配[0－9a-zA-Z_]
  // $1是与正则表达式中的第 1 个子表达式相匹配的文本
  // 如modulePath:./tagsView.js 匹配后就是tagsView
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const value = modulesFiles(modulePath)
  modules[moduleName] = value.default
  return modules
}, {})

export default createStore({
  modules,
  getters,
});

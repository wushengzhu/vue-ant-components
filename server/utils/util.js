const fs = require('fs');
const fst = require('fs-extra');
class Util {
  /**
   * 判断是否为undfined、null
   * @param value
   * @returns
   */
  static isUndefinedOrNull (value) {
    return typeof value === "undefined" || value === null;
  }

  /**
   * 判断是否为undefined、null或仅有空白字符
   */
  static isUndefinedOrNullOrWhiteSpace (value) {
    return (
      typeof value === "undefined" || value === null || /^\s*$/.test(value)
    );
  }

  /**
   * 判断是否为0或空白字符等
   */
  static isZeroOrWhiteSpace (value) {
    if (typeof value === "undefined" || value === null) {
      return true;
    } else if (typeof value === "string") {
      return /^\s*$/.test(value) || value === "0";
    } else if (typeof value === "number") {
      return value === 0;
    } else {
      return false;
    }
  }

  /**
   * 判断是否为整数
   * @param value
   */
  static isInt (value) {
    return typeof value === "number" && value % 1 === 0;
  }

  /**
   * 判断数组是否为Null或者空
   */
  static IsNullOrEmpty (array) {
    if (!Util.isUndefinedOrNull(array)) {
      if (array instanceof Array) {
        if (array.length > 0) {
          return false;
        }
      }
    }
    return true;
  }
}

const resolvePost = async (req) =>
  new Promise(resolve => {
    let chunk = ""
    // req.on(data) 每次发送的数据
    req.on("data", data => {
      chunk += data
    })
    console.log('dkahj', chunk);
    // req.on(end),数据发送完成
    req.on("end", () => {
      resolve(chunk)
      // resolve(JSON.parse(chunk))
    })
  })


// 返回已经上传切片名列表
const getUploadedList = async (dirPath) => {
  return fst.existsSync(dirPath)
    ? (await fst.readdir(dirPath)).filter(name => name[0] !== '.') // 过滤诡异的隐藏文件
    : []
}

// 截取扩展格式符号，如.jpg
const extractExt = filename => filename.slice(filename.lastIndexOf("."), filename.length)

const pipeStream = (filePath, writeStream) =>
  new Promise(resolve => {
    const readStream = fs.createReadStream(filePath)
    readStream.on("end", () => {
      // 删除临时文件夹当前分片内容
      fs.unlinkSync(filePath)
      resolve()
    })
    readStream.pipe(writeStream)
  })

// files 分片路径数组
// dest 合成文件存储路径
// size 分片长度
mergeFiles = async (files, dest, size) => {
  await Promise.all(
    files.map((file, index) =>
      // 遍历分片写入
      pipeStream(
        file,
        // 指定位置创建可写流 加一个put避免文件夹和文件重名
        // hash后不存在这个问题，因为文件夹没有后缀
        // fse.createWriteStream(path.resolve(dest, '../', 'out' + filename), {
        // 快速创建可写流，以将数据写入文件
        fs.createWriteStream(dest, {
          start: index * size,
          end: (index + 1) * size
        })
      )
    )
  )
}

const Utils = { Util, resolvePost, getUploadedList, extractExt, mergeFiles }

module.exports = Utils;

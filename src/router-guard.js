import router from './router'
import store from './store'

router.beforeEach(async (to, from, next) => {

    await store.dispatch('permission/generateRoutes', []).then(() => {
        next();
    })
})

router.afterEach(() => {

})

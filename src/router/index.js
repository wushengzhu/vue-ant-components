import { createRouter, createWebHistory } from 'vue-router';
import Home from '@/views/Home.vue';

export const customRoutes = [
  {
    // 首页
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/home',
    children: [
      {
        path: 'home',
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/Home.vue'),
        name: 'home',
        meta: {
          title: '首页',
          affix: true,
        }
      },
    ],
  },
  {
    path: '/list',
    name: 'list',
    meta: {
      title: '列表组件',
    },
    component: () => import('@/views/TableList.vue'),
  },
  {
    path: '/upload',
    name: 'upload',
    meta: {
      title: '上传组件',
    },
    component: () => import('@/views/UploadFile.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: customRoutes,
});

export default router;

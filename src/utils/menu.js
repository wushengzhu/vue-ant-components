const menuList = [
  {
    link: '/home',
    title: '首页',
    icon: 'home',
    key: '1',
  },
  {
    link: '/list',
    title: '列表组件',
    icon: 'home',
    key: '2',
  },
  {
    link: '/upload',
    title: '上传组件',
    icon: 'upload',
    key: '3',
  },
  // {
  //   link: '/dictionary',
  //   title: '字典组件',
  //   icon: 'upload',
  //   key: '4',
  // },
  // {
  //   link: '/log',
  //   title: '日志组件',
  //   icon: 'log',
  //   key: '5',
  // },
];

export default menuList;
